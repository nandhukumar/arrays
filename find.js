// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.

function find(elements, cb) {
  if (!Array.isArray(elements) || !elements || !cb) {
    return undefined;
  }
  for (let c = 0; c < elements.length; c++) {
    let item = elements[c];
    if (cb(item, c, elements)) {
      return item;
    }
  }
  return undefined;
}

module.exports = find;
