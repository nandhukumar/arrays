// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function reduce(elements, cb, startValue) {
  if (!Array.isArray(elements) || !cb || !elements) {
    return [];
  }
  if (startValue === undefined) {
    startValue = elements[0];
  } else {
    startValue = cb(startValue, elements[0]);
  }
  for (let c = 1; c < elements.length; c++) {
    let item = elements[c];
    startValue = cb(startValue, item);
  }

  return startValue;
}

module.exports = reduce;
