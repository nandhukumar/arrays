// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

const { __esModule } = require("async");

function map(elements = [], cb = undefined) {
  if ((!cb) || (!(Array.isArray(elements))) || (!elements)) {
      return [];
  }
  else {
      let array = [];
      for(c=0; c<elements.length; c++) {
          let item = elements[c];
          let mapping = cb(item, c, elements);
          array.push(mapping)
      }
      return array
  }
}

module.exports = map;
