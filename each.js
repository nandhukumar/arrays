// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument

function reduce(elements, cb) {
  if (!Array.isArray(elements) || !cb || !elements) {
    return [];
  }
  for (let c = 0; c < elements.length; c++) {
    let item = elements[c];
    elements[c] = cb(item, c, elements);
  }
  return elements;
}

module.exports = reduce;
