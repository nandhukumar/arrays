let items = [1, 2, 3, 4, 5, 5];

let reduce = require("../reduce");

function cb(first, second) {
  return first + second;
}

let startValue = 5;

let result = reduce(items, cb, startValue);
console.log(result);