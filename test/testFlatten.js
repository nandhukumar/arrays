const flattenFn = require("../flatten.js")

const array = [1, [2], [[3]], [[[4]]]]


const result = flattenFn(array)

console.log(result)
